// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TestObject.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the TestObject type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Reimers.EsentStore.Tests
{
	internal class TestObject
	{
		public string Name { get; set; }
		
		public int Value { get; set; }
	}
}