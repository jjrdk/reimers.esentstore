﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CacheCleanerTests.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the CacheCleanerTests type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Reimers.EsentStore.Tests
{
	using System;
	using System.Collections.Generic;
	using System.Threading.Tasks;
	using NUnit.Framework;

    public class CacheCleanerTests
	{
		public class GivenACacheCleaner : IDisposable
		{
			private readonly CacheCleaner<int> _sut;
			private readonly Dictionary<int, Tuple<DateTime, string>> _store;

			public GivenACacheCleaner()
			{
				_store = new Dictionary<int, Tuple<DateTime, string>>();
				_sut = new CacheCleaner<int>(TimeSpan.FromSeconds(0.5), _store);
			}

			/// <summary>
			/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
			/// </summary>
			public void Dispose()
			{
				_sut.Dispose();
			}

			[Test]
			public async Task WhenCleanerCleansCacheThenRemovesOldItems()
			{
				_store.Add(1, new Tuple<DateTime, string>(DateTime.UtcNow, "test"));

				await Task.Delay(TimeSpan.FromSeconds(1.5));

				Assert.IsEmpty(_store);
			}
		}
	}
}
