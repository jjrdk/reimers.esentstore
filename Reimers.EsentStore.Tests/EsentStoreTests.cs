// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EsentStoreTests.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the EsentStoreTests type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Reimers.EsentStore.Tests
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using NUnit.Framework;

    public sealed class EsentStoreTests
    {
        public sealed class GivenAnEsentStore
        {
            private Store<int> _sut;
            private const string StoreName = "test";

            [SetUp]
            public void SetUp()
            {
                _sut = new Store<int>(StoreName);
            }

            [TearDown]
            public void Teardown()
            {
                _sut.Dispose();
                var userAppData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                var storePath = Path.Combine(userAppData, "EsentStore", StoreName);
                if (Directory.Exists(storePath))
                {
                    Directory.Delete(storePath, true);
                }
            }

            [Test]
            public void WhenNoItemInStoreThenGettingByKeyReturnsDefaultValue()
            {
                var item = _sut.Get<string>(1);

                Assert.AreEqual(default(string), item);
            }

            [Test]
            public void WhenSavingAnonymousItemThenCanReadTypedItemWithSameSignature()
            {
                _sut.Put(1, new { Name = "Item", Value = 2 });

                var found = _sut.Get<TestObject>(1);

                Assert.IsInstanceOf<TestObject>(found);
            }

            [Test]
            public void CanStoreFiftyThousandItemsPerSecond()
            {
                var items = Enumerable.Range(0, 50000).Select(x => new { Name = "Item " + x, Value = x }).ToArray();
                var stopwatch = new Stopwatch();
                stopwatch.Start();

                foreach (var item in items)
                {
                    _sut.Put(item.Value, item);
                }

                stopwatch.Stop();
                Assert.LessOrEqual(stopwatch.ElapsedTicks, TimeSpan.TicksPerSecond);
            }
        }

        public sealed class GivenAPersistentQueue
        {
            private PersistentQueue<TestObject> _sut;
            private const string StoreName = "queue";

            [SetUp]
            public void SetUp()
            {
                _sut = new PersistentQueue<TestObject>(StoreName, TimeSpan.FromSeconds(3));
            }

            [TearDown]
            public void Teardown()
            {
                var userAppData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                var storePath = Path.Combine(userAppData, "EsentStore", StoreName);
                if (Directory.Exists(storePath))
                {
                    try
                    {
                        Directory.Delete(storePath, true);
                    }
                    catch (IOException) { }
                }
            }

            [Test]
            public void CanProcessFiftyThousandItemsPerSecond()
            {
                var stopwatch = new Stopwatch();
                stopwatch.Start();

                var count = 35000;
                var enqueueTasks = Enumerable.Range(0, count)
                    .Select(
                    x => new
                        TestObject
                    {
                        Name = "Item " + x,
                        Value = x
                    })
                    .Select(x => Task.Run(() => _sut.Enqueue(x)))
                    .ToArray();

                for (int i = 0; i < count; i++)
                {
                    var item = _sut.Dequeue();
                }

                stopwatch.Stop();
                Console.WriteLine(((stopwatch.ElapsedTicks * 100) / (double)TimeSpan.TicksPerSecond) + "%");
                Assert.LessOrEqual(stopwatch.ElapsedTicks, TimeSpan.TicksPerSecond);
            }
        }
    }
}