﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Store.cs" company="Reimers.dk">
//   Copyright © Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the Store type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Reimers.EsentStore
{
    using System;
    using System.Collections.Concurrent;
    using System.IO;
    using System.Threading;
    using Microsoft.Isam.Esent.Collections.Generic;
    using Newtonsoft.Json;

    public class Store<TKey> : IDisposable
        where TKey : struct, IComparable<TKey>
    {
        private readonly PersistentDictionary<TKey, string> _innerDictionary;
        private readonly ConcurrentDictionary<TKey, Tuple<DateTime, string>> _innerCache = new ConcurrentDictionary<TKey, Tuple<DateTime, string>>();
        private readonly CacheCleaner<TKey> _cleaner;

        public Store(string name)
            : this(name, TimeSpan.FromMinutes(5))
        {
        }

        public Store(string name, TimeSpan cacheTimeout)
        {
            var userAppData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var esentStorePath = Path.Combine(userAppData, "EsentStore");
            if (!Directory.Exists(esentStorePath))
            {
                Directory.CreateDirectory(esentStorePath);
            }

            var storePath = Path.Combine(esentStorePath, name);

            _innerDictionary = new PersistentDictionary<TKey, string>(storePath);
            _cleaner = new CacheCleaner<TKey>(cacheTimeout, _innerCache);
        }

        public TItem Get<TItem>(TKey key)
        {
            if (_innerDictionary.ContainsKey(key))
            {
                Tuple<DateTime, string> cached;
                string json;
                if (_innerCache.TryGetValue(key, out cached))
                {
                    json = cached.Item2;
                }
                else
                {
                    lock (_innerDictionary)
                    {
                        _innerDictionary.TryGetValue(key, out json);
                    }
                }

                if (!string.IsNullOrWhiteSpace(json))
                {
                    UpdateCache(key, json);
                    return JsonConvert.DeserializeObject<TItem>(json);
                }

                Remove(key);
            }

            return default(TItem);
        }

        public void Put<TItem>(TKey key, TItem item)
        {
            var json = JsonConvert.SerializeObject(item);
            _innerDictionary.Add(key, json);
            _innerCache.AddOrUpdate(
                key,
                new Tuple<DateTime, string>(DateTime.UtcNow, json),
                (k, v) => new Tuple<DateTime, string>(DateTime.UtcNow, v.Item2));
        }

        public void Remove(TKey key)
        {
            lock (_innerDictionary)
            {
                Tuple<DateTime, string> cached;
                _innerDictionary.Remove(key);
                _innerCache.TryRemove(key, out cached);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void UpdateCache(TKey key, string json)
        {
            _innerCache.AddOrUpdate(key, new Tuple<DateTime, string>(
                DateTime.UtcNow, json),
                (k, v) => new Tuple<DateTime, string>(DateTime.UtcNow, json));
        }

        private void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                _innerDictionary.Dispose();
                _cleaner.Dispose();
                _innerCache.Clear();
            }
        }
    }
}
