namespace Reimers.EsentStore
{
    using System;
    using System.Collections.Generic;
    using System.Threading;

    public class PersistentQueue<T>
    {
        private long _id;
        private readonly Queue<long> _queue = new Queue<long>();
        private readonly Store<long> _store;

        public PersistentQueue(string name, TimeSpan cacheTimeout)
        {
            _store = new Store<long>(name, cacheTimeout);
        }

        public void Enqueue(T item)
        {
            lock (_store)
            {
                var number = _id++;
                _queue.Enqueue(number);
                _store.Put(number, item);
                Monitor.Pulse(_store);
            }
        }

        public T Dequeue()
        {
            lock (_store)
            {
                while (_queue.Count == 0)
                {
                    Monitor.Wait(_store);
                }
                var id = _queue.Dequeue();
                var item = _store.Get<T>(id);
                _store.Remove(id);
                return item;
            }
        }
    }
}