// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CacheCleaner.cs" company="Reimers.dk">
//   Copyright � Reimers.dk 2014
//   This source is subject to the Microsoft Public License (Ms-PL).
//   Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
//   All other rights reserved.
// </copyright>
// <summary>
//   Defines the CacheCleaner type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Reimers.EsentStore
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Threading;

	internal class CacheCleaner<TKey> : IDisposable
	{
		private readonly IDictionary<TKey, Tuple<DateTime, string>> _cache;
		private readonly Timer _timer;
		private readonly double _interval;

		public CacheCleaner(TimeSpan cleanupInterval, IDictionary<TKey, Tuple<DateTime, string>> cache)
		{
			_cache = cache;
			_interval = Math.Min(cleanupInterval.TotalMilliseconds, TimeSpan.FromDays(1).TotalMilliseconds);
			_timer = new Timer(OnElapsed, null, cleanupInterval, cleanupInterval);
		}

		public void Dispose()
		{
			_timer.Dispose();
		}

		private void OnElapsed(object state)
		{
			var latest = DateTime.UtcNow.AddMilliseconds(-_interval);
			var toDelete = _cache.Where(x => x.Value.Item1 <= latest).Select(x => x.Key).ToArray();
			foreach (var key in toDelete)
			{
				_cache.Remove(key);
			}
		}
	}
}